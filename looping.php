<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        /* 
            Soal No 1 
            Looping I Love PHP
            Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
            dan Looping yang ke dua menurun (Descending). 

            Output: 
            LOOPING PERTAMA
            2 - I Love PHP
            4 - I Love PHP
            6 - I Love PHP
            8 - I Love PHP
            10 - I Love PHP
            12 - I Love PHP
            14 - I Love PHP
            16 - I Love PHP
            18 - I Love PHP
            20- I Love PHP
            LOOPING KEDUA
            20 - I Love PHP
            18 - I Love PHP
            16 - I Love PHP
            14 - I Love PHP
            12 - I Love PHP
            10 - I Love PHP
            8 - I Love PHP
            6 - I Love PHP
            4 - I Love PHP
            2 - I Love PHP
        */
        // Lakukan Looping Di Sini


for ($i=1; $i <=20; $i++){
if ($i%2==0) {
    echo "$i - I Love PHP <br>"; 
}
}

for ($i=20; $i>=2; $i--){
    if ($i%2==0){
        echo "$i - I Love PHP <br>";
    }
}



?>







        <h3>Soal No 2 Looping Array Modulo </h3>
        
<?php
        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        // Lakukan Looping di sini
echo "<br>";
echo "Hasil sisa dari array numbers :";
foreach($numbers as $value){
    $rest[] = $value%5;
}
echo "<br>";
print_r($rest)


        ?>


        <h3> Soal No 3 Looping Asociative Array </h3>
        
        <?php

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        foreach ($items as $key => $value) {

$no = array (
    'id' => $value [0],
    'name' => $value [1],
    'price' => $value [2],
    'description' => $value [3],
    'source' => $value [4] 
);
print_r($no);
echo "<br>";

        }

?>


        
        echo <h3>Soal No 4 Asterix </h3>
        
        <?php
        for ($i=1;$i<=5;$i++){
            for ($j=$i;$j>=1;$j--){
                echo "*";
            } echo "<br>";
        }
        
    ?>

</body>
</html>
